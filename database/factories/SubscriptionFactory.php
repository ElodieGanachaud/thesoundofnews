<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

//use App\Model;
use App\Subscription;
use Faker\Generator as Faker;

$factory->define(Subscription::class, function (Faker $faker) {
    /*dd($faker->randomFloat(2));*/
    return [
        'price' => $faker->randomFloat(2, 20, 200),
        'duration' => '2020-01-10'
    ];
});
