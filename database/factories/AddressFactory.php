<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

//use App\Model;
use App\Address;
use Faker\Generator as Faker;

$factory->define(Address::class, function (Faker $faker) {
    return [
        'address' => '3 rue de la paix',
        'zip_code' => '72000',
        'city' => 'Le Mans',
        'country' => 'France'
    ];
});
