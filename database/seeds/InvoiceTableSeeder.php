<?php

use Illuminate\Database\Seeder;

class InvoiceTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Invoice::class, 5)->create()->each(function($invoice) {
            $invoice->user()->associate(App\User::inRandomOrder('')->first());
            $invoice->save();
        });
    }
}
