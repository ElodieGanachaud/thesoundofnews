<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RoleTableSeeder::class);
        $this->call(UserTableSeeder::class);
        $this->call(CategoryTableSeeder::class);
        $this->call(TagTableSeeder::class);
        $this->call(ArticleTableSeeder::class);
        $this->call(CommentTableSeeder::class);
        $this->call(ThreadTableSeeder::class);
        $this->call(MessageTableSeeder::class);
        $this->call(SubscriptionTableSeeder::class);
        $this->call(InvoiceTableSeeder::class);
        $this->call(AddressesTableSeeder::class);
        $this->call(ReportTableSeeder::class);
    }
}
