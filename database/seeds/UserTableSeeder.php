<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\User::class, 5)->create()->each(function($user) {
            $user->role()->associate(App\Role::inRandomOrder('')->first());
            $user->save();
        });

        $admin = new \App\User([
            'name' => 'Administrateur',
            'email' => 'admin@admin.com',
            'email_verified_at' => now(),
            'password' => '$2y$10$0GwndyMmx8dkQCofC475zei8dfznETY8jquYlPBw3y4nGiqeqnt/K',
            'salt' => Str::random(10),
            'renew' => 0,
            'token' => Str::random(10),
            'active' => 1,
            'remember_token' => Str::random(10),
            'role_id' => 1,
        ]);
        $admin->save();

        $redac = new \App\User([
            'name' => 'Rédacteur',
            'email' => 'redac@redac.com',
            'email_verified_at' => now(),
            'password' => '$2y$10$0GwndyMmx8dkQCofC475zei8dfznETY8jquYlPBw3y4nGiqeqnt/K',
            'salt' => Str::random(10),
            'renew' => 0,
            'token' => Str::random(10),
            'active' => 1,
            'remember_token' => Str::random(10),
            'role_id' => 2,
        ]);
        $redac->save();

        $premium = new \App\User([
            'name' => 'Premium',
            'email' => 'premium@premium.com',
            'email_verified_at' => now(),
            'password' => '$2y$10$0GwndyMmx8dkQCofC475zei8dfznETY8jquYlPBw3y4nGiqeqnt/K',
            'salt' => Str::random(10),
            'renew' => 0,
            'token' => Str::random(10),
            'active' => 1,
            'remember_token' => Str::random(10),
            'role_id' => 3,
        ]);
        $premium->save();

        $normal = new \App\User([
            'name' => 'Normal',
            'email' => 'normal@normal.com',
            'email_verified_at' => now(),
            'password' => '$2y$10$0GwndyMmx8dkQCofC475zei8dfznETY8jquYlPBw3y4nGiqeqnt/K',
            'salt' => Str::random(10),
            'renew' => 0,
            'token' => Str::random(10),
            'active' => 1,
            'remember_token' => Str::random(10),
            'role_id' => 4,
        ]);
        $normal->save();
    }
}
