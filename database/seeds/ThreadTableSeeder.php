<?php

use Illuminate\Database\Seeder;

class ThreadTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Thread::class, 10)->create()->each(function($thread) {
            $thread->user()->associate(App\User::inRandomOrder('')->first());
            $thread->save();
        });
    }
}
