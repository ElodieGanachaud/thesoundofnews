<?php

use Illuminate\Database\Seeder;

class AddressesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Address::class, 1)->create()->each(function($address) {
            $address->user()->associate(App\User::inRandomOrder('')->first());
            $address->save();
        });
    }
}
