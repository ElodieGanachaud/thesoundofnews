<?php

use Illuminate\Database\Seeder;

class CommentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Comment::class, 50)->create()->each(function($comment) {
            $comment->article()->associate(App\Article::inRandomOrder('')->first());
            $comment->user()->associate(App\User::inRandomOrder('')->first());
            $comment->save();
        });
    }
}
