<?php

use Illuminate\Database\Seeder;

class ReportTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Report::class, 1)->create()->each(function($report) {
            $report->user()->associate(App\User::inRandomOrder('')->first());
            $report->save();
        });
    }
}
