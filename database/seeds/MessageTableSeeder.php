<?php

use Illuminate\Database\Seeder;

class MessageTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Message::class, 100)->create()->each(function($message) {
            $message->thread()->associate(App\Thread::inRandomOrder('')->first());
            $message->user()->associate(App\User::inRandomOrder('')->first());
            $message->save();
        });
    }
}
