<?php

use Illuminate\Database\Seeder;

class ArticleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Article::class, 10)->create()->each(function($article) {
            $article->category()->associate(App\Category::inRandomOrder('')->first());
            $article->user()->associate(App\User::inRandomOrder('')->first());
            $article->tags()->attach(App\Tag::inRandomOrder('')->take(2)->get());
            $article->save();
        });
    }
}
