<?php

use Illuminate\Database\Seeder;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Role::create(['title' => 'Administrateur']);
        App\Role::create(['title' => 'Rédacteur']);
        App\Role::create(['title' => 'Premium']);
        App\Role::create(['title' => 'Normal']);
    }
}
