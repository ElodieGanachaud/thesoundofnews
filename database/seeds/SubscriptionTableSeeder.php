<?php

use Illuminate\Database\Seeder;

class SubscriptionTableSeeder extends Seeder
{
    /**
         * Run the database seeds.
         *
         * @return void
         */
        public function run()
        {
            factory(App\Subscription::class, 1)->create()->each(function($subscription) {
                $subscription->user()->associate(App\User::inRandomOrder('')->first());
                $subscription->save();
            });
        }
}
