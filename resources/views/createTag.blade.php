@include('layouts.app')

<div class="container">
    <h2>Créer un tag</h2>
    <br>

    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    {!!  Form::open() !!}
    <div class="form-group">
        <label for="">Titre</label>
        {{ Form::text('title', null, ['class' => 'form-control']) }}
    </div>
    {!! Form::submit('Valider', ['class' => 'btn btn-success float-right']) !!}
    {!!  Form::close() !!}
</div>

</body>
</html>
