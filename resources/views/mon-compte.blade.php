@include('layouts.app')

<div class="container">
    <h2 class="text-center">Mon compte</h2>

    <br>

    <div class="row font-size-17">
        <div class="font-weight-bold">{!! \App\User::findOrFail(Auth::user()->id)->name !!}</div>&nbsp;
        <div class="text-muted">(inscrit depuis le {{ \App\User::findOrFail(Auth::user()->id)->created_at }})</div>
    </div>

    <div class="row font-size-17">Statut : {{$statut}}</div>

    <div class="row font-size-17">
        <p>Nombre de messages postés sur le forum : <span class="font-weight-bold">{{ \App\User::findOrFail(Auth::user()->id)->messages()->count() }}</span></p>
    </div>

    <div class="row font-size-17">
        <p>
            Abonnement :
            @if((\App\User::findOrFail(Auth::user()->id)->subscription()->count() == 0) || (\App\User::findOrFail(Auth::user()->id)->subscription()->first()->duration < date('Y-m-d')))
                Aucun abonnement.</p></div>
                <div class="row font-size-17">
                    <a href="/subscribe"><div class="btn btn-primary">M'abonner</div></a>
                </div>

                <br>
            @else
                Il vous reste {{ $dateDiff }} jours d'abonnement.</p></div>
            @endif

    @if(\App\User::findOrFail(Auth::user()->id)->address != null)
        <div class="row font-size-17">
            <a href="/adresse/edit/{{ \App\User::findOrFail(Auth::user()->id)->address()->first()->id }}">
                <p>Changer mon adresse</p>
            </a>
        </div>
    @endif
    <?php $factures = \App\Invoice::where('user_id', '=', Auth::user()->id)->get(); ?>
    @if($factures->first() != null)
    <div class="row font-size-17">
        <a href="/factures">
            <p>Voir mes factures</p>
        </a>
    </div>
    @endif




</div>
</body>
</html>

@include('templates.footer')
