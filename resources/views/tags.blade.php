@include('layouts.app')

<div class="container">
    <h2 class="text-center">Tags</h2>

    <br>

    @if((Auth::user() != null) && (Auth::user()->role_id <= 2))
    <a href="http://127.0.0.1:8000/tags/create"><div class="btn btn-dark">Créer un tag</div></a>

    <br><br>
    @endif



    <div class="row">
        @foreach ($tags as $tag)
            <div class="col-sm-3 pb-4 pr-2">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title"><a href="/tags/show/{{$tag->slug}}" class="text-dark">{{$tag->title}}</a></h5>
                        @if((Auth::user() != null) && (Auth::user()->role_id <= 2))
                            <div class="row justify-content-start">
                                <div class="pl-3">
                                    <a href="/tags/edit/{{$tag->id}}"><div class="btn btn-primary">Éditer</div></a>
                                </div>
                                <div class="pl-2">
                                    {!! Form::open(['route' => ['tags.delete', $tag->id], 'method' => 'DELETE']) !!}
                                    {{ Form::submit('Supprimer', ['class' => 'btn btn-danger']) }}
                                    {!! Form::close() !!}
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        @endforeach
    </div>
</div>

</body>
</html>

@include('templates.footer')
