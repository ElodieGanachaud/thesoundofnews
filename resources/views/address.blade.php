@include('layouts.app')

<div class="container">
    <h2>Votre adresse</h2>
    <br>

    {!!  Form::open() !!}
    <div class="form-group">
        <label for="">Adresse</label>
        {{ Form::text('address', null, ['class' => 'form-control']) }}
    </div>
    <div class="form-group">
        <label for="">Code postal</label>
        {{ Form::text('zip_code', null, ['class' => 'form-control']) }}
    </div>
    <div class="form-group">
        <label for="">Ville</label>
        {{ Form::text('city', null, ['class' => 'form-control']) }}
    </div>
    <div class="form-group">
        <label for="">Pays</label>
        {{ Form::text('country', null, ['class' => 'form-control']) }}
    </div>
    {!! Form::submit('Valider', ['class' => 'btn btn-success float-right']) !!}
    {!!  Form::close() !!}
</div>

</body>
</html>

@include('templates.footer')
