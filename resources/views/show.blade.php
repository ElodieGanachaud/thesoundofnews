@include('layouts.app')

<div class="container">
    <div class="pull-left">
        Catégorie : <a href="/categories/show/{{\App\Category::findOrFail($article->category_id)->slug}}">{{\App\Category::find($article->category_id)->title}}</a>
    </div>

    <br>

    <article class="text-center">
        <h2>{{$article->title}}</h2>
        <br>
        <p>{{$article->content}}</p>
        <?php
        \App\Article::findOrFail($article->id)->tags()->each(function($tag) {
            echo '<a href="/tags/show/'.\App\Tag::findOrFail($tag->id)->slug.'" class="float-right pl-2">#'.$tag->title.'</a> ';
        });
        ?>
    </article>

    <br><br><br><br><br>

    <div class="row m-auto">
        <h3>Commentaires :</h3>
        @if((Auth::user() != null))
            <a href="http://127.0.0.1:8000/comments/create/{{$article->id}}" class="ml-auto"><div class="btn btn-dark">Ajouter un commentaire</div></a>
        @endif
    </div>

    <?php $comments = DB::table('comments')->where('article_id', $article->id)->get(); ?>

    @foreach($comments as $comment)
        <div class="row">
            <div class="col-lg-12 pt-3">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">{{\App\User::find($comment->user_id)->name}}</h5>
                        <p class="card-text">{{$comment->content}}</p>

                        <div class="row justify-content-end">
                            <!-- Si c'est la personne qui a écrit le commentaire il peut le modifier ou le supprimer -->
                            @if((Auth::user() != null) && (Auth::user()->id == $comment->user_id))
                                    <div class="pr-3">
                                        <a href="/comments/edit/{{$comment->id}}"><div class="btn btn-primary">Éditer</div></a>
                                    </div>
                                    <div class="pr-2">
                                        {!! Form::open(['route' => ['comments.delete', $comment->id], 'method' => 'DELETE']) !!}
                                        {{ Form::submit('Supprimer', ['class' => 'btn btn-danger']) }}
                                        {!! Form::close() !!}
                                    </div>
                            @endif
                            <div class="pr-3">
                                <a href="/report/1/{{$comment->id}}"><div class="btn btn-danger">Signaler</div></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endforeach


</div>

</body>
</html>

@include('templates.footer')
