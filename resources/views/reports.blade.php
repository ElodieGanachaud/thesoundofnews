@include('layouts.app')

<div class="container">
    <h2 class="text-center">Reports</h2>

    <br>

    <div class="row">
        @foreach($reports as $report)
            <div class="col-lg-12 pb-4 pr-2">
                <div class="card">
                    <div class="card-body text-center">
                        {!! Form::open(['route' => ['reports.delete', $report->id], 'method' => 'DELETE']) !!}
                        {{ Form::submit('Supprimer', ['class' => 'btn  btn-sm btn-danger pull-right']) }}
                        {!! Form::close() !!}

                        @if($report->type == 1)
                            <a class="text-dark" href="/articles/show/{{ \App\Comment::findOrFail($report->id_content)->article()->first()->slug }}">{{ \App\User::findOrFail($report->user_id)->name }} - {{ Str::limit(\App\Comment::findOrFail($report->id_content)->content, 100) }}</a>
                        @else
                            <a class="text-dark" href="/thread/show/{{ \App\Message::findOrFail($report->id_content)->thread()->first()->slug }}">{{ \App\User::findOrFail($report->user_id)->name }} - {{ Str::limit(\App\Message::findOrFail($report->id_content)->content, 100) }}</a>
                        @endif
                    </div>
                </div>
            </div>
        @endforeach
    </div>

</div>
</body>
</html>

@include('templates.footer')
