@include('layouts.app')

<div class="container">
    <h2 class="text-center">Forum</h2>

    <br>

    @if((Auth::user() != null))
        <a href="http://127.0.0.1:8000/thread/create"><div class="btn btn-dark">Ajouter un sujet</div></a>

        <br><br>
    @endif

    <div class="row">
        @foreach($threads as $thread)
        <div class="col-lg-12 pb-4 pr-2">
            <div class="card">
                <div class="card-body">
                    <div class="row justify-content-between">
                        <div class="col-lg-4 text-left my-auto">
                            <a href="/thread/show/{{$thread->slug}}" class="text-dark font-weight-bold">{{$thread->title}}</a>
                            <div class="text-muted">Par {{\App\User::find($thread->user_id)->name}}, le {{$thread->created_at}}</div>
                        </div>
                        <div class="col-lg-4 text-center my-auto">{{DB::table('messages')->where('thread_id', $thread->id)->count()}} messages</div>
                        <div class="col-lg-4 text-right my-auto text-muted">
                            Dernier message écrit par {{$thread->lastMessage['user']['name']}}<br>Le {{$thread->lastMessage['created_at']}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endforeach
    </div>
</body>
</html>

@include('templates.footer')
