@include('layouts.app')

<div class="container">
    <h2 class="text-center">Mes factures</h2>

    <br>

    <div class="row">
        @foreach ($factures as $facture)
            <div class="col-lg-12 pb-4 pr-2">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title my-auto"><a href="/facture/{{$facture->id}}" class="text-dark">Facture du {{$facture->created_at}} - {{$facture->item}}</a></h5>
                    </div>
                </div>
            </div>
        @endforeach
    </div>

</div>
</body>
</html>

@include('templates.footer')
