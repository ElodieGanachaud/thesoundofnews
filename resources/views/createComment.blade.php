@include('layouts.app')

<div class="container">
    <h2>Ajouter un commentaire</h2>
    <br>

    {!!  Form::open() !!}
    <div class="form-group">
        {{ Form::textarea('content', null, ['class' => 'form-control']) }}
    </div>
    {!! Form::submit('Valider', ['class' => 'btn btn-success float-right']) !!}
    {!!  Form::close() !!}
</div>

</body>
</html>
