@include('layouts.app')

<div class="container">
    <h2 class="text-center">Catégories</h2>

    <br>

    @if((Auth::user() != null) && (Auth::user()->role_id <= 2))
    <a href="http://127.0.0.1:8000/categories/create"><div class="btn btn-dark">Créer une catégorie</div></a>

    <br><br>
    @endif


    <div class="row">
        @foreach ($categories as $categorie)
            <div class="col-sm-3 pb-4 pr-2">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title"><a href="/categories/show/{{$categorie->slug}}" class="text-dark">{{$categorie->title}}</a></h5>
                        <p class="card-text"></p>

                        @if((Auth::user() != null) && (Auth::user()->role_id <= 2))
                            <div class="row justify-content-start">
                                <div class="pl-3">
                                    <a href="/categories/edit/{{$categorie->id}}"><div class="btn btn-primary">Éditer</div></a>
                                </div>
                                <div class="pl-2">
                                    {!! Form::open(['route' => ['categories.delete', $categorie->id], 'method' => 'DELETE']) !!}
                                    {{ Form::submit('Supprimer', ['class' => 'btn btn-danger']) }}
                                    {!! Form::close() !!}
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        @endforeach
    </div>

</div>

</body>
</html>

@include('templates.footer')
