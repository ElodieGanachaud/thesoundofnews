@include('layouts.app')

<div class="container">
    <h2>Créer un article</h2>
    <br>

    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    {!!  Form::open() !!}
    <div class="form-group">
        <label for="">Catégorie</label>
        <?php $categories = \App\Category::all(); ?>
        {{ Form::select('category_id',  $categories->pluck("title","id"), 1, ['class' => 'form-control']) }}
    </div>
    <div class="form-group">
        <label for="">Tag</label>
        <?php $tags = \App\Tag::all(); ?>
        {{ Form::select('tag_id[]', $tags->pluck("title","id"), null, ['class' => 'form-control', 'multiple' => 'multiple']) }}
    </div>
    <div class="form-group">
        <label for="">Titre</label>
        {{ Form::text('title', null, ['class' => 'form-control']) }}
    </div>
    <div class="form-group">
        <label for="">Contenu</label>
        {{ Form::textarea('content', null, ['class' => 'form-control']) }}
    </div>
    <div class="form-group">
        <label for="">Premium</label>
        {!! Form::checkbox('premium', '1') !!}
        &nbsp;
        <label for="">Published</label>
        {!! Form::checkbox('published', '1') !!}
    </div>
    {!! Form::submit('Valider', ['class' => 'btn btn-success float-right']) !!}
    {!!  Form::close() !!}
</div>

</body>
</html>

@include('templates.footer')
