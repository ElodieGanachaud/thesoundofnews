@include('layouts.app')

<div class="container">
    <h2 class="text-center">Mon compte</h2>

    <br>

    <div class="row justify-content-between">
        <div class="card card-hover text-white text-center bg-primary mb-3 card-center" style="max-width: 18rem; min-width: 18rem; min-height: 10rem;">
            <a class="card-link my-auto" href="/subscribe/3">
                <div class="card-body">
                    <h5 class="text-white font-weight-bold">3 MOIS</h5>
                    <p class="text-white">2.99€ / mois</p>
                </div>
            </a>
        </div>

        <div class="card card-hover text-white text-center bg-success mb-3 card-center" style="max-width: 18rem; min-width: 18rem; min-height: 10rem;">
            <a class="card-link my-auto" href="/subscribe/6">
                <div class="card-body">
                    <h5 class="text-white font-weight-bold">6 MOIS</h5>
                    <p class="text-white">4.99€ / mois</p>
                </div>
            </a>
        </div>

        <div class="card card-hover text-white text-center bg-danger mb-3 card-center" style="max-width: 18rem; min-width: 18rem; min-height: 10rem;">
            <a class="card-link my-auto" href="/subscribe/12">
                <div class="card-body">
                    <h5 class="text-white font-weight-bold">12 MOIS</h5>
                    <p class="text-white">7.99€ / mois</p>
                </div>
            </a>
        </div>
    </div>



</div>
</body>
</html>

@include('templates.footer')
