@include('layouts.app')

<div class="container">
    <h2>Créer un sujet</h2>
    <br>

    {!!  Form::open() !!}
    <div class="form-group">
        <label for="">Titre</label>
        {{ Form::text('title', null, ['class' => 'form-control']) }}
    </div>
    <div class="form-group">
        <label for="">Message</label>
        {{ Form::textarea('content', null, ['class' => 'form-control']) }}
    </div>
    {!! Form::submit('Valider', ['class' => 'btn btn-success float-right']) !!}
    {!!  Form::close() !!}
</div>

</body>
</html>
