@include('layouts.app')

<div class="container">

    <h2 class="text-center">Articles liés au tag #{{ $tag->title }}</h2>

    <br><br>

    <div class="row">
        @foreach ($articles as $article)
            <div class="col-sm-3 pb-4 pl-1 pr-2">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title"><a href="/articles/show/{{\App\Article::findOrFail($article->id)->slug}}" class="text-dark">{{$article->title}}</a></h5>
                        <p class="card-text">{{Str::limit($article->content, 255)}}</p>
                        <p><?php
                            $article->tags()->each(function($tag) {
                                echo '<a href="/tags/show/'.\App\Tag::findOrFail($tag->id)->slug.'">#'.$tag->title.'</a> ';
                            });
                            ?>
                        </p>
                        <div class="row justify-content-start">
                            @if((Auth::user() != null) && (Auth::user()->role_id <= 3) && (Auth::user()->id == $article->user_id))
                            <div class="pl-3">
                                <a href="/articles/edit/{{$article->id}}"><div class="btn btn-primary">Éditer</div></a>
                            </div>
                            <div class="pl-2">
                                {!! Form::open(['route' => ['articles.delete', $article->id], 'method' => 'DELETE']) !!}
                                {{ Form::submit('Supprimer', ['class' => 'btn btn-danger']) }}
                                {!! Form::close() !!}
                            </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    </div>

</div>
</body>
</html>

@include('templates.footer')
