@include('layouts.app')

<div class="container">
    <h2 class="text-center">Facture du {{$facture->created_at}}</h2>

    <br>

    <address>
        {{ $adresse->address }} <br>
        {{ $adresse->zip_code }} {{$adresse->city}} <br>
        {{ $adresse->country }}
    </address>

    <br>

    <table class="table table-striped">
        <thead class="thead-dark">
        <tr>
            <th scope="col">#</th>
            <th scope="col">Item</th>
            <th scope="col">Prix</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <th scope="row">{{$facture->id}}</th>
            <td>{{$facture->item}}</td>
            <td>{{$facture->price}}€</td>
        </tr>
        </tbody>
    </table>

    <br>

    <h3 class="pull-right">Total : {{$facture->price}}€</h3>

</div>
</body>
</html>

@include('templates.footer')
