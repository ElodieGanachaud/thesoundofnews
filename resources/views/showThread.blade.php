@include('layouts.app')

<div class="container">

    <h2 class="text-center">{{ $thread->title }}</h2>

    <br>

    <div class="row">
            <div class="col-sm-12 pb-4 pr-2">
                <div class="card">
                    <div class="card-body">
                        <div class="row m-auto">
                            <h5 class="card-title">{{\App\User::find($thread->user_id)->name}}</h5>
                            <p class="ml-auto text-muted">{{$thread->created_at}}</p>
                        </div>
                        <p class="card-text">{{$thread->content}}</p>
                    </div>
                </div>
            </div>
    </div>

    <br><br>


    @if((Auth::user() != null))
        <a href="http://127.0.0.1:8000/message/create/{{$thread->id}}"><div class="btn btn-dark">Ajouter un message</div></a>

        <br><br>
    @endif

    <div class="row">
        @foreach ($messages as $message)
            <div class="col-sm-12 pb-4 pr-2">
                <div class="card">
                    <div class="card-body">

                        <div class="row m-auto">
                            <h5 class="card-title">{{\App\User::findOrFail($message->user_id)->name}}</h5>
                            <p class="ml-auto text-muted">{{$message->created_at}}</p>
                        </div>
                        <p class="card-text">{{$message->content}}</p>

                        <!-- Si c'est la personne qui a écrit le message il peut le modifier ou le supprimer. Ou s'il est administrateur il peut seulement le supprimer. -->
                        @if((Auth::user() != null) && ((Auth::user()->id == $message->user_id) || ((Auth::user()->role_id == 1))))
                            <div class="row justify-content-end">
                                @if((Auth::user() != null) && (Auth::user()->id == $message->user_id))
                                <div class="pr-3">
                                    <a href="/message/edit/{{$message->id}}"><div class="btn btn-primary">Éditer</div></a>
                                </div>
                                @endif
                                <div class="pr-2">
                                    {!! Form::open(['route' => ['messages.delete', $message->id], 'method' => 'DELETE']) !!}
                                    {{ Form::submit('Supprimer', ['class' => 'btn btn-danger']) }}
                                    {!! Form::close() !!}
                                </div>
                                <div class="pr-3">
                                    <a href="/report/2/{{$message->id}}"><div class="btn btn-danger">Signaler</div></a>
                                </div>
                            </div>
                        @endif

                    </div>
                </div>
            </div>
        @endforeach
    </div>


</div>
</body>
</html>

@include('templates.footer')
