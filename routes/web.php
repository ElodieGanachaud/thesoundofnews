<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Article;

Route::get('/', function () {
    return view('welcome');
});

Route::get('/articles', 'ArticleController@index')->name('articles_show')->middleware('checkip');

//Route::get('/articles/{id}', 'ArticleController@show')->where('id', '[0-9]+')->name('article_show');

Route::get('/articles/show/{slug}', 'ArticleController@show')->name('article_show');

Route::get('/articles/create', 'ArticleController@create')->middleware('checkrole:Premium');

Route::get('/articles/edit/{id}', 'ArticleController@edit')->where('id', '[0-9]+')->middleware('checkrole:Premium');

Route::post('articles/edit/{id}', 'ArticleController@update');

Route::post('/articles/create', 'ArticleController@store');

Route::delete('/articles/{id}', 'ArticleController@delete')->where('id', '[0-9]+')->name('articles.delete')->middleware('checkrole:Premium');


Route::get('/categories', 'CategoryController@index')->name('categories_show');

Route::get('/categories/show/{slug}', 'CategoryController@show')->name('categorie_show');

Route::get('/categories/create', 'CategoryController@create')->middleware('checkrole:Rédacteur');

Route::post('/categories/create', 'CategoryController@store');

Route::get('/categories/edit/{id}', 'CategoryController@edit')->where('id', '[0-9]+')->middleware('checkrole:Rédacteur');

Route::post('categories/edit/{id}', 'CategoryController@update');

Route::delete('/categories/{id}', 'CategoryController@delete')->where('id', '[0-9]+')->name('categories.delete')->middleware('checkrole:Rédacteur');


Route::get('/tags', 'TagController@index')->name('tags_show');

Route::get('/tags/show/{slug}', 'TagController@show')->name('tag_show');

Route::get('/tags/create', 'TagController@create')->middleware('checkrole:Rédacteur');

Route::post('/tags/create', 'TagController@store');

Route::get('/tags/edit/{id}', 'TagController@edit')->where('id', '[0-9]+')->middleware('checkrole:Rédacteur');

Route::post('tags/edit/{id}', 'TagController@update');

Route::delete('/tags/{id}', 'TagController@delete')->where('id', '[0-9]+')->name('tags.delete')->middleware('checkrole:Rédacteur');


Route::get('/comments/create/{id}', 'CommentController@create')->where('id', '[0-9]+')->middleware('checkrole:Normal');

Route::post('/comments/create/{id}', 'CommentController@store')->where('id', '[0-9]+');

Route::get('/comments/edit/{id}', 'CommentController@edit')->where('id', '[0-9]+')->middleware('checkrole:Normal');

Route::post('comments/edit/{id}', 'CommentController@update');

Route::delete('/comments/{id}', 'CommentController@delete')->where('id', '[0-9]+')->name('comments.delete')->middleware('checkrole:Normal');


Route::get('/forum', 'ThreadController@index')->middleware('checkrole:Premium');

Route::get('/thread/show/{slug}', 'ThreadController@show')->name('thread_show');

Route::get('/thread/create', 'ThreadController@create')->middleware('checkrole:Normal');

Route::post('/thread/create', 'ThreadController@store');


Route::get('/message/create/{id}', 'MessageController@create')->where('id', '[0-9]+')->middleware('checkrole:Normal');

Route::post('/message/create/{id}', 'MessageController@store')->where('id', '[0-9]+');

Route::get('/message/edit/{id}', 'MessageController@edit')->where('id', '[0-9]+')->middleware('checkrole:Normal');

Route::post('message/edit/{id}', 'MessageController@update');

Route::delete('/message/{id}', 'MessageController@delete')->where('id', '[0-9]+')->name('messages.delete')->middleware('checkrole:Normal');

Route::get('/mon-compte', 'CompteController@index')->where('id', '[0-9]+')->name('mon-compte')->middleware('checkrole:Normal');

Route::get('/subscribe', 'CompteController@subscribe_page')->where('id', '[0-9]+')->name('subscribe')->middleware('checkrole:Normal');

Route::get('/subscribe/{id}', 'CompteController@subscribe')->where('id', '[0-9]+')->middleware('checkrole:Normal');

Route::get('/adresse', function () { return view('address'); })->middleware('checkrole:Normal');

Route::post('/adresse', 'CompteController@adresse')->name('adresse')->middleware('checkrole:Normal');

Route::get('/adresse/edit/{id}', 'CompteController@edit_adresse')->where('id', '[0-9]+')->middleware('checkrole:Normal');

Route::post('/adresse/edit/{id}', 'CompteController@update_adresse');

Route::get('/factures', 'InvoiceController@index')->middleware('checkrole:Normal');

Route::get('/facture/{id}', 'InvoiceController@show')->where('id', '[0-9]+')->middleware('checkrole:Normal');

Route::get('/report/{type}/{id}', 'ReportController@store')->where('id', '[0-9]+')->middleware('checkrole:Normal');

Route::get('/reports', 'ReportController@index')->name('reports')->middleware('checkrole:Administrateur');

Route::delete('/reports/{id}', 'ReportController@delete')->where('id', '[0-9]+')->name('reports.delete')->middleware('checkrole:Administrateur');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
