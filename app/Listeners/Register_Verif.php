<?php

namespace App\Listeners;

use App\Mail\ShowMail;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;

class Register_Verif
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        try {
            Mail::send(new ShowMail($event->user));
        } catch (\Exception $e) {
            report($e);
            return false;
        }
    }
}
