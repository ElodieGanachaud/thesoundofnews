<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class Thread extends Model
{
    protected $fillable = ['title', 'slug', 'content', 'user_id'];

    public function setTitleAttribute($data) {
        $this->attributes['title'] = $data;
        $this->attributes['slug'] = Str::slug($data);
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function messages(){
        return $this->hasMany(Message::class);
    }

    public function lastMessage(){
        return $this->hasOne(Message::class)->latest();
    }


}
