<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Str;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'salt', 'renew', 'token', 'active', 'remember_token', 'role_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function setNameAttribute($data) {
        $this->attributes['name'] = $data;
        $this->attributes['salt'] = Str::random(10);
        $this->attributes['renew'] = '0';
        $this->attributes['token'] = Str::random(10);
        $this->attributes['active'] = '1';
        $this->attributes['remember_token'] = Str::random(10);
        $this->attributes['role_id'] = '4';
    }

    public function article()
    {
        return $this->hasMany('App\Article');
    }

    public function role() {
        return $this->belongsTo('App\Role');
    }

    public function category() {
        return $this->hasMany('App\Category');
    }

    public function comments()
    {
        return $this->hasMany('App\Comment');
    }

    public function threads() {
        return $this->hasMany('App\Thread');
    }

    public function messages() {
        return $this->hasMany('App\Message');
    }

    public function subscription() {
        return $this->hasOne('App\Subscription');
    }

    public function invoice() {
        return $this->hasOne('App\Invoice');
    }

    public function address() {
        return $this->hasOne('App\Address');
    }

    public function hasRole($role) {
        switch($role) {
            case 'Administrateur':
                if($this->role_id == 1) {
                    return true;
                }
                break;

            case 'Rédacteur':
                if($this->role_id <= 2) {
                    return true;
                }
                break;
            case 'Premium':
                if($this->role_id <= 3) {
                    return true;
                }
                break;
            case 'Normal':
                if($this->role_id <= 4) {
                    return true;
                }
                break;
        }
    }

    public static function getStatut($statut) {
        if($statut == 1) {
            return 'Administrateur';
        } else if($statut == 2) {
            return 'Rédacteur';
        } else if($statut == 3) {
            return 'Premium';
        } else if($statut == 4) {
            return 'Normal';
        }
    }
}
