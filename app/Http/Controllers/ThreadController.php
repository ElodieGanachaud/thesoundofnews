<?php

namespace App\Http\Controllers;

use App\Message;
use App\Thread;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ThreadController extends Controller
{
    public function index()
    {
        $threads = Thread::with('lastMessage.user')->get();
        return view('forum', ['threads' => $threads]);
    }

    public function show($slug) {
        $thread = DB::table('threads')->where('slug', $slug)->get()->first();
        $messages = DB::table('messages')->where('thread_id', $thread->id)->orderBy('created_at', 'asc')->get();
        return view('showThread', ['messages' => $messages, 'thread' => $thread]);
    }

    public function create() {
        return view('createThread');
    }

    public function store(Request $request) {
        Thread::create($request->all() + ['user_id' => $request->user()->id]);

        //$thread = Thread::find($request->id)->slug;
        //return redirect()->route('thread_show', ['slug' => $thread]);
    }
}
