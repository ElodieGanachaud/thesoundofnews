<?php

use App\Article;
use Illuminate\Http\Request;

namespace App\Http\Controllers;

use App\Article;
use App\ArticleAddRequest;
use App\ArticleEditRequest;
use App\Events\DeleteArticle;
use App\Mail\ShowMail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class ArticleController extends Controller
{
    public function index()
    {
        $articles = Article::all();
        return view('article', ['articles' => $articles]);
    }

    public function show($slug, Request $request) {
        //Je cherche tous les articles qui ont le slug passé en paramètre, et je renvoie sur la page de l'article
        $article = DB::table('articles')->where('slug', $slug)->get()->first();
        return view('show', ['article' => $article]);
    }

    public function create() {
        return view('create');
    }

    public function store(ArticleAddRequest $request) {
        /* Selon si $request->premium et $request->published existent, je crée l'article avec différents paramètres
        Si $request->premium et $request->published n'existent pas, alors ils sont initialisés à 0. S'ils existent ils sont dans $request et sont égaux à 1. */
        if(!isset($request->premium) && (!isset($request->published))) {
            $article = Article::create($request->all() + ['user_id' => $request->user()->id] + ['premium' => '0'] + ['published' => '0']);
        } else if(!isset($request->premium)) {
            $article = Article::create($request->all() + ['user_id' => $request->user()->id] + ['premium' => '0']);
        } else if(!isset($request->published)) {
            $article = Article::create($request->all() + ['user_id' => $request->user()->id] + ['published' => '0']);
        } else {
            $article = Article::create($request->all() + ['user_id' => $request->user()->id]);
        }

        //On attache les tags à article
        $article->tags()->attach($request->tag_id);

        return redirect()->route('article_show', ['slug' => $article->slug]);
    }

    public function edit($id) {
        //Si c'est l'utilisateur connecté qui a écrit l'article
        if((Auth::user()->id) == (Article::findOrFail($id)->user_id)) {
            //On cherche l'article dont l'id est passé en paramètre et on renvoie la page edit avec l'article passé en paramètre
            $article = Article::findOrFail($id);
            return view('edit', ['article' => $article]);
        } else {
            return redirect()->route('articles_show');
        }
    }

    public function update($id, ArticleEditRequest $request) {
        /* Selon si $request->premium et $request->published existent, je crée l'article avec différents paramètres
        Si $request->premium et $request->published n'existent pas, alors ils sont initialisés à 0. S'ils existent ils sont dans $request et sont égaux à 1. */
        if(!isset($request->premium) && (!isset($request->published))) {
            $article = Article::find($id)->update($request->all() + ['user_id' => $request->user()->id] + ['premium' => '0'] + ['published' => '0']);
        } else if(!isset($request->premium)) {
            $article = Article::find($id)->update($request->all() + ['user_id' => $request->user()->id] + ['premium' => '0']);
        } else if(!isset($request->published)) {
            $article = Article::find($id)->update($request->all() + ['user_id' => $request->user()->id] + ['published' => '0']);
        } else {
            $article = Article::find($id)->update($request->all() + ['user_id' => $request->user()->id]);
        }

        Article::find($id)->title = $request->title;
        //On enlève tous les tags de l'article et on ajoute tous les tags présents dans la requête
        Article::find($id)->tags()->detach(Article::find($id)->tags);
        if(isset($request->tag_id)) {
            foreach ($request->tag_id as $tag) {
                Article::find($id)->tags()->attach($tag);
            }
        }
        $article = Article::find($id)->slug;
        return redirect()->route('article_show', ['slug' => $article]);
    }

    public function delete($id) {
        echo Article::findOrFail($id)->delete();
        //event(new DeleteArticle);
        return redirect()->route('articles_show');
    }
}
