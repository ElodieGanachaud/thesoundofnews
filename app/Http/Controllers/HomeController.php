<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        //Si abonnement expiré & user premium alors le repasser en normal
        if((User::findOrFail(Auth::user()->id)->subscription()->first()) != null) {
            if ((User::findOrFail(Auth::user()->id)->subscription()->first()->duration < date('Y-m-d')) && (Auth::user()->role_id == 3)) {
                User::where('id', '=', Auth::user()->id)->first()->update(['role_id' => 4]);
            }
        }
        return view('home');
    }
}
