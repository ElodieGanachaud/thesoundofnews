<?php

namespace App\Http\Controllers;

use App\Category;
use App\CategoryAddRequest;
use App\CategoryEditRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::all();
        return view('categorie', ['categories' => $categories]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return view('createCategory');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CategoryAddRequest $request) {
        $categorie = Category::create($request->all());
        return redirect()->route('categorie_show', ['slug' => $categorie->slug]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        //On cherche la catégorie dont le slug est égal à celui passé en paramètre
        $categorie = DB::table('categories')->where('slug', $slug)->get()->first();
        //On cherche les articles correspondant à la catégorie
        $articles = Category::findOrFail($categorie->id)->articles;

        return view('showCategory', ['articles' => $articles, 'categorie' => $categorie]);
    }

    public function edit($id) {
        $categorie = Category::findOrFail($id);
        return view('editCategory', ['categorie' => $categorie]);
    }

    public function update($id, CategoryEditRequest $request) {
        Category::find($id)->update($request->all());
        return redirect()->route('categories_show');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function delete($id) {
        echo Category::findOrFail($id)->delete();
        return redirect()->route('categories_show');
    }
}
