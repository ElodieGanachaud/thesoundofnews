<?php

namespace App\Http\Controllers;

use App\Invoice;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class InvoiceController extends Controller
{
    public function index() {
        $statut = User::getStatut(Auth::user()->role_id);
        $subscription = \App\User::findOrFail(Auth::user()->id)->subscription()->first();
        if($subscription != null) {
            $dateSubscription = date_create($subscription->duration);
            $dateNow = date_create(date('Y-m-d'));
            $dateDiff = date_diff($dateSubscription, $dateNow)->days;
        } else {
            $dateDiff = 0;
        }

        $factures = Invoice::where('user_id', '=', Auth::user()->id)->get();
        if($factures->first() != null) {
            if ($factures->first()->user_id == Auth::user()->id) {
                return view('factures', ['factures' => $factures]);
            }
        } else {
            return view('mon-compte', ['statut' => $statut, 'dateDiff' => $dateDiff]);
        }
    }

    public function show($id) {
        $statut = User::getStatut(Auth::user()->role_id);
        $subscription = \App\User::findOrFail(Auth::user()->id)->subscription()->first();
        if($subscription != null) {
            $dateSubscription = date_create($subscription->duration);
            $dateNow = date_create(date('Y-m-d'));
            $dateDiff = date_diff($dateSubscription, $dateNow)->days;
        } else {
            $dateDiff = 0;
        }

        $facture = Invoice::findOrFail($id);
        if($facture->user_id == Auth::user()->id) {
            $adresse = User::findOrFail(Auth::user()->id)->address()->first();
            return view('show_facture', ['facture' => $facture, 'adresse' => $adresse]);
        } else {
            return view('mon-compte', ['statut' => $statut, 'dateDiff' => $dateDiff]);
        }
    }
}
