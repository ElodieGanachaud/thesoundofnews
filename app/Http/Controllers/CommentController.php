<?php

namespace App\Http\Controllers;

use App\Article;
use App\Comment;
//use App\ArticleAddRequest;
//use App\ArticleEditRequest;
use App\Mail\ShowMail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class CommentController extends Controller
{

    public function create($id) {
        return view('createComment', ['id' => $id]);
    }

    public function store($id, Request $request) {
        //On crée le commentaire en ajoutant l'article et l'utilisateur dans celui-ci
        Comment::create($request->all() + ['article_id' =>$id] + ['user_id' => $request->user()->id]);

        //On retrouve le slug de l'article pour renvoyer sur cette page
        $article = Article::find($id)->slug;
        return redirect()->route('article_show', ['slug' => $article]);
    }

    public function edit($id) {
        $comment = Comment::findOrFail($id);
        //Si c'est bien l'utilisateur connecté qui a écrit l'article
        if((Auth::user()->id) == (Comment::findOrFail($id)->user_id)) {
            return view('editComment', ['comment' => $comment]);
        } else {
            return redirect()->route('article_show', ['slug' => $comment->article()->first()->slug]);
        }
    }

    public function update($id, Request $request) {
        Comment::find($id)->update($request->all());
        $article = Comment::find($id)->article_id;
        $article = Article::find($article)->slug;
        return redirect()->route('article_show', ['slug' => $article]);
    }

    public function delete($id) {
        $article = Comment::find($id)->article_id;
        $article = Article::find($article)->slug;
        Comment::findOrFail($id)->delete();
        return redirect()->route('article_show', ['slug' => $article]);
    }
}
