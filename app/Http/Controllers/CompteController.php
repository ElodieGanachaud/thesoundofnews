<?php

namespace App\Http\Controllers;

use App\Address;
use App\Invoice;
use App\User;
use App\Subscription;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CompteController extends Controller
{
    public function index() {
        $statut = User::getStatut(Auth::user()->role_id);
        $subscription = \App\User::findOrFail(Auth::user()->id)->subscription()->first();
        if($subscription != null) {
            $dateSubscription = date_create($subscription->duration);
            $dateNow = date_create(date('Y-m-d'));
            $dateDiff = date_diff($dateSubscription, $dateNow)->days;
        } else {
            $dateDiff = 0;
        }

        return view('mon-compte', ['statut' => $statut, 'dateDiff' => $dateDiff]);
    }

    public function subscribe_page() {
        //Si je n'ai pas d'adresse...
        if(\App\User::findOrFail(Auth::user()->id)->address == null) {
            return redirect()->route('adresse');
        } else {
            return view('subscribe');
        }
    }

    public function subscribe($id) {
        $statut = User::getStatut(Auth::user()->role_id);
        $subscription = \App\User::findOrFail(Auth::user()->id)->subscription()->first();
        if($subscription != null) {
            $dateSubscription = date_create($subscription->duration);
            $dateNow = date_create(date('Y-m-d'));
            $dateDiff = date_diff($dateSubscription, $dateNow)->days;
        } else {
            $dateDiff = 0;
        }

        $date = date('Y-m-d', strtotime("+{$id} months", strtotime(date('Y-m-d'))));
        $price = 0;

        if($id == 3) {
            $price = 2.99;
        } else if($id == 6) {
            $price = 4.99;
        } else if($id == 12) {
            $price = 7.99;
        }

        //S'il n'y a pas de subscription de user, alors créer. (! prix)
        if(Subscription::where('user_id', '=', Auth::user()->id)->count() == 0) {
            Subscription::create(['price' => $price, 'duration' => $date, 'user_id' => Auth::user()->id]);
        } else {
            //Sinon mise à jour de la date + prix
            Subscription::where('user_id', '=', Auth::user()->id)->first()->update(['duration' => $date]);
        }

        //Faire une facture
        Invoice::create(['item' => "{$id} mois", 'price' => $price, 'user_id' => Auth::user()->id]);
        //Si le user est un user normal, le passer en premium
        if(Auth::user()->role_id == 4) {
            User::where('id', '=', Auth::user()->id)->first()->update(['role_id' => 3]);
        }

        return redirect()->route('mon-compte');
    }

    public function adresse(Request $request) {
        Address::create($request->all() + ['user_id' => $request->user()->id]);
        return redirect()->route('subscribe');
    }

    public function edit_adresse($id) {
        $adresse = Address::findOrFail($id);
        return view('editAddress', ['adresse' => $adresse]);
    }

    public function update_adresse($id, Request $request) {
        Address::find($id)->update($request->all() + ['user_id' => $request->user()->id]);
        return redirect()->route('mon-compte');
    }
}
