<?php

use App\Report;
use Illuminate\Http\Request;

namespace App\Http\Controllers;

use App\Report;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ReportController extends Controller
{
    public function index() {
        $reports = Report::all();
        return view('reports', ['reports' => $reports]);
    }

    public function store($type, $id) {
        Report::create(['type' => $type] + ['id_content' => $id] + ['user_id' => Auth::user()->id]);
        return view('reportEffectue');
    }

    public function delete($id) {
        Report::findOrFail($id)->delete();
        return redirect()->route('reports');
    }
}
