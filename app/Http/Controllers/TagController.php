<?php

use App\Tag;

namespace App\Http\Controllers;

use App\Article;
use App\Tag;
use App\TagAddRequest;
use App\TagEditRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TagController extends Controller
{
    public function index()
    {
        $tags = Tag::all();
        return view('tags', ['tags' => $tags]);
    }

    public function show($slug) {
        //On cherche le tag qui a le slug passé en paramètre, et on renvoie la vue qui affiche le tag
        $tag = DB::table('tags')->where('slug', $slug)->get()->first();
        return view('tagShow', ['tag' => $tag, 'articles' => Tag::findOrFail($tag->id)->articles]);
    }

    public function create() {
        return view('createTag');
    }

    public function store(TagAddRequest $request) {
        //Si le tag n'existe pas, alors on le crée. On renvoie vers la page du tag dans tous les cas.
        if(Tag::where('title', '=', $request->title)->first() == null) {
            $tag = Tag::create($request->all());
        } else {
            $tag = Tag::where('title', '=', $request->title)->first();
        }
        return redirect()->route('tag_show', ['slug' => $tag->slug]);
    }

    public function edit($id) {
        $tag = Tag::findOrFail($id);
        return view('editTag', ['tag' => $tag]);
    }

    public function update($id, TagEditRequest $request) {
        Tag::find($id)->update($request->all());
        $slug = Tag::find($id)->slug;
        return redirect()->route('tags_show', ['slug' => $slug]);
    }

    public function delete($id) {
        echo Tag::findOrFail($id)->delete();
        return redirect()->route('tags_show');
    }
}
