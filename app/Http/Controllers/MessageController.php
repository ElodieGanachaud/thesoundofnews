<?php

namespace App\Http\Controllers;

use App\Message;
use App\Thread;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class MessageController extends Controller
{

    public function create($id) {
        return view('createMessage', ['id' => $id]);
    }

    public function store($id, Request $request) {
        Message::create($request->all() + ['thread_id' =>$id] + ['user_id' => $request->user()->id]);
        $thread = Thread::find($id)->slug;
        return redirect()->route('thread_show', ['slug' => $thread]);
    }

    public function edit($id) {
        $message = Message::findOrFail($id);
        //Si c'est bien l'utilisateur connecté qui a écrit l'article
        if((Auth::user()->id) == (Message::findOrFail($id)->user_id)) {
            return view('editMessage', ['message' => $message]);
        } else {
            return redirect()->route('thread_show', ['slug' => $message->thread()->first()->slug]);
        }
    }

    public function update($id, Request $request) {
        Message::find($id)->update($request->all());
        $thread = Message::find($id)->thread_id;
        $thread = Thread::find($thread)->slug;
        return redirect()->route('thread_show', ['slug' => $thread]);
    }

    public function delete($id) {
        $thread = Message::find($id)->thread_id;
        $thread = Thread::find($thread)->slug;
        Message::findOrFail($id)->delete();
        return redirect()->route('thread_show', ['slug' => $thread]);
    }
}
