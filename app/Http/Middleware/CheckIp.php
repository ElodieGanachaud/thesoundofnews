<?php

namespace App\Http\Middleware;

use Closure;

class CheckIp
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $ips = ['127.0.0.1', '128.1.1.2'];
        //if($request->server->all()['REMOTE_ADDR'] == "127.0.0.1") {
        if(count(array_intersect($request->getClientIps(), $ips))) {
            return $next($request);
        } else {
            abort('403', 'Unauthorized action');
        }
    }
}
