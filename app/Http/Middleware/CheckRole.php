<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class CheckRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $role)
    {
        if(!(auth()->check())) {
            return redirect()->route('login');
        } else {
            $user = $request->user();
            if(!$user->hasRole($role)) {
                abort('403', 'Accès refusé');
            }
        }

        return $next($request);
    }
}
