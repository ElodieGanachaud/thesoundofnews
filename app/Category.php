<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Category extends Model
{
    protected $fillable = ['title'];

    public function setTitleAttribute($data) {
        $this->attributes['title'] = $data;
        $this->attributes['slug'] = Str::slug($data);
    }

    public function articles()
    {
        return $this->hasMany('App\Article');
    }

    public function user() {
        return $this->belongsTo('App\User');
    }
}
