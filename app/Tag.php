<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Tag extends Model
{
    protected $fillable = ['title'];

    public function setTitleAttribute($data) {
        $this->attributes['title'] = $data;
        $this->attributes['slug'] = Str::slug($data);
    }

    public function articles()
    {
        return $this->belongsToMany('App\Article');
    }
}
