<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    protected $fillable = ['item', 'price', 'user_id'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
