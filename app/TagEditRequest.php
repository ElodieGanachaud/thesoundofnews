<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Http\FormRequest;

class TagEditRequest extends FormRequest
{
    public function rules() {
        return [
            'title' =>  'required|max:100'
        ];
    }


    public function messages() {
        return [
            'title.required' => 'Le titre est obligatoire',
            'title.max' => 'Le titre ne doit pas dépasser 100 caractères'
        ];
    }
}
