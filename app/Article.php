<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Article extends Model
{
    protected $fillable = ['title', 'slug','premium', 'content', 'published', 'category_id', 'user_id'];

    public function setTitleAttribute($data) {
        $this->attributes['title'] = $data;
        $this->attributes['slug'] = Str::slug($data);
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /*public function tag()
    {
        return $this->hasMany('App\Tag');
    }*/

    public function category()
    {
        return $this->belongsTo('App\Category');
    }

    public function tags() {
        return $this->belongsToMany('App\Tag');
    }

    public function comments()
    {
        return $this->hasMany('App\Comment');
    }
}
